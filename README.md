About
=====

[CastURL.com](http://www.casturl.com/) is a single-utility tool for casting video, music, or image URLs to any [Chromecast](http://www.google.com/chrome/devices/chromecast/) with one click.